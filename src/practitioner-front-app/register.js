import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-form/iron-form.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';

class CustomerRegister extends PolymerElement {
  static get template() {
    return html`
        <iron-form>
            <paper-input name="firstName" always-float-label label="Nombres" required="true" value={{customer.firstName}}></paper-input>
            <paper-input name="lastName" always-float-label label="Apellidos" required="true" value={{customer.lastName}}></paper-input>
            <paper-input name="email" always-float-label label="Correo Electrónico" type="email" required="true" value={{customer.email}}></paper-input>
            <paper-input name="password" always-float-label label="Contraseña" type="password" required="true" value={{customer.password}}></paper-input>
            <paper-button raised on-click="registerCustomer">Registrar</paper-button>
            <paper-spinner id="indicatorLoading"></paper-spinner>
        </iron-form>
    `;
  }
  static get is() { return 'customer-register'; }
  static get properties() {
    return {
        customer: { type: Object, value: function() { return {} } },
        isLoading: { type: Boolean, notify: true },
        apiBaseUrl : { type: String }
    }
  }
  registerCustomer() {
    this.isLoading = true;
    console.log(this.customer);
    var url = this.apiBaseUrl + 'customers';
    let _this = this;
    var appHeaders = new Headers();
    appHeaders.append("Content-Type", "application/json");
    appHeaders.append("X-App-Session", "NA");//This must be removed in next version
    fetch(url, {
        method: 'POST', 
        body: JSON.stringify(this.customer), 
        headers:appHeaders
    }).then(function(response) {
        if(response.ok) {
            return response.json();
        }
        throw new Error('Network response was not ok.');
    }).then(function(responseJson) { 
        console.log(responseJson);
        _this.dispatchEvent(
            new CustomEvent('show-message', {
                detail: {
                    textModal: 'Usuario registrado: ' + responseJson.id +'   El password de activación será enviado al correo: ' + responseJson.email
                }
            }
        ));
        _this.isLoading = false;
    }).catch(function(error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
        _this.isLoading = false;
    });
  }
}
customElements.define(CustomerRegister.is, CustomerRegister);