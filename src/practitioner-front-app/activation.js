import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-form/iron-form.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';

class CustomerActivation extends PolymerElement {
  static get template() {
    return html`
        <iron-form>
            <paper-input always-float-label label="ID Cliente" required="true" value={{requestActivate.customerId}}></paper-input>
            <paper-input always-float-label label="Contraseña de activación" required="true" type="password" value={{requestActivate.temporalPassword}}></paper-input>
            <paper-button raised on-click="activateCustomer">Activar</paper-button>
        </iron-form>
    `;
  }
  static get properties() {
    return {
        requestActivate: { type: Object, value: function() { return {} } },
        isLoading: { type: Boolean, notify: true },
        apiBaseUrl : { type: String }
    }
  }
  activateCustomer() {
    this.isLoading = true;
    this.requestActivate.customerId = parseInt(this.requestActivate.customerId);
    console.log('activateCustomer(input): ' + JSON.stringify(this.requestActivate));
    var url = this.apiBaseUrl + 'customers/activate';
    let _this = this;
    var appHeaders = new Headers();
    appHeaders.append("Content-Type", "application/json");
    fetch(url, {
        method: 'POST', 
        body: JSON.stringify(this.requestActivate), 
        headers:appHeaders
    }).then(function(response) {
        if(response.ok) {
            return response.json();
        }
        throw new Error('Network response was not ok.');
    }).then(function(responseJson) { 
        console.log('activateCustomer(output): ' + JSON.stringify(responseJson));
        _this.dispatchEvent(
            new CustomEvent('show-message', {
                detail: {
                    textModal: 'Usuario activado!'
                }
            }
        ));
        _this.isLoading = false;
    }).catch(function(error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
        _this.isLoading = false;
    });
  }
}
customElements.define('customer-activation', CustomerActivation);