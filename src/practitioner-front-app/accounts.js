import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@polymer/paper-item/paper-item.js';

class AccountsMod extends PolymerElement {
  static get template() {
    return html`
        <vaadin-combo-box id="accounts" 
            label="Cuentas" 
            item-label-path="description" 
            item-value-path="id" items="[[accounts]]" as="item">
            <template>
                <paper-item-body two-line style="min-height: 0">
                    <div style="text-transform: capitalize">[[item.id]]</div>
                    <div secondary>[[item.description]]</div>
                </paper-item-body>
            </template>
        </vaadin-combo-box>
        <paper-button raised on-click="getAccounts">Actualizar</paper-button>
        <paper-input always-float-label label="Descripción Cuenta" value="{{account}}"></paper-input> 
        <paper-button raised on-click="addAccount">Alta</paper-button>   
    `;
  }
  static get is() { return 'accounts-mod'; }
  static get properties() {
    return {
        objCustomer: { type: Object },
        accounts:    { type: Array },
        account:     { type: String },
        idSession:   { type: String },
        isLoading:   { type: Boolean, notify: true },
        apiBaseUrl : { type: String }
    }
  }
  getAccounts() {
    this.isLoading = true;  
    console.log('getAccounts(input): ' + JSON.stringify(this.objCustomer));
    var url = this.apiBaseUrl + 'customers/' + this.objCustomer.id + '/accounts';
    var appHeaders = new Headers();
    appHeaders.append("Content-Type", "application/json");
    appHeaders.append("X-App-Session", this.idSession);
    let _this = this;
    fetch(url, {
        method: 'GET', 
        headers:appHeaders
    }).then(function(response) {
        if(response.ok) {
            return response.json();
        }
        throw new Error('Network response was not ok.');
    }).then(function(responseJson) { 
        console.log('getAccounts(response): ' + JSON.stringify(responseJson));
        //combobox.items = responseJson;
        _this.accounts = responseJson;
        _this.isLoading = false;
    }).catch(function(error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
        _this.isLoading = false;
    });
  }
  addAccount() {
    this.isLoading = true;  
    console.log('addAccount(input): ' + JSON.stringify(this.account));
    var url = this.apiBaseUrl + 'customers/' + this.objCustomer.id + '/accounts';
    var appHeaders = new Headers();
    appHeaders.append("Content-Type", "application/json");
    appHeaders.append("X-App-Session", this.idSession);
    let _this = this;
    fetch(url, {
        method: 'POST', 
        body: JSON.stringify({'description':this.account}), 
        headers:appHeaders
    }).then(function(response) {
        if(response.ok) {
            return response.json();
        }
        throw new Error('Network response was not ok.');
    }).then(function(responseJson) { 
        console.log('addAccount(response): ' + JSON.stringify(responseJson));
        _this.account = '';
        _this.isLoading = false;
        _this.getAccounts();
    }).catch(function(error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
        _this.isLoading = false;
    });
  }
}
customElements.define(AccountsMod.is, AccountsMod);