import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-form/iron-form.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';

class CustomerLogin extends PolymerElement {
  static get template() {
    return html`
        <iron-form>
            <paper-input always-float-label label="ID Cliente" required="true" value={{requestSession.customerId}}></paper-input>
            <paper-input always-float-label label="Password" type="password" required="true" value={{requestSession.password}}></paper-input>
            <paper-button raised on-click="loginCustomer">Ingresar</paper-button>
        </iron-form>
    `;
  }
  static get is() { return 'customer-login'; }
  static get properties() {
    return {
        requestSession: { type: Object, value: function() { return {} } },
        idSession: { type: String, notify: true },
        isLoading: { type: Boolean, notify: true },
        apiBaseUrl : { type: String }
    }
  }
  loginCustomer() {
    this.isLoading = true;
    this.requestSession.customerId = parseInt(this.requestSession.customerId);
    console.log('loginCustomer(input): ' + JSON.stringify(this.requestSession));
    let _this = this;
    var url = this.apiBaseUrl + 'session';
    var appHeaders = new Headers();
    appHeaders.append("Content-Type", "application/json");
    fetch(url, {
        method: 'POST', 
        body: JSON.stringify(this.requestSession), 
        headers:appHeaders
    }).then(function(response) {
        if(response.ok) {
            return response.json();
        }
        throw new Error('Network response was not ok.');
    }).then(function(responseJson) { 
        console.log('loginCustomer(output): ' + JSON.stringify(responseJson));
        console.log('Login success, save session in localStorage');
        localStorage.setItem('sessionApp', responseJson.session);
        _this.idSession = responseJson.session;
        _this.dispatchEvent(
            new CustomEvent('show-message', {
                detail: {
                    textModal: 'Acceso correcto, sesión generada!',
                }
            }
        ));
        _this.isLoading = false;
    }).catch(function(error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
        _this.isLoading = false;
    });
  }
}
customElements.define(CustomerLogin.is, CustomerLogin);