import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/paper-spinner/paper-spinner.js';
import '@polymer/paper-dialog/paper-dialog.js';

/**
 * @customElement
 * @polymer
 */
class PractitionerFrontApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        app-header {
          background-color: #072146;
          color: #CCCCCC;
        }
        paper-tabs {
          background-color: #004481;
          color: #CCCCCC;
        }
        iron-pages {
          display: flex;
          flex-direction: column;
          align-items: center;
        }
      </style>
      <app-drawer-layout>
        <app-header-layout>
          <app-header>
            <app-toolbar>
              <div main-title>Practitioner Front App</div>
              <paper-spinner active$="[[isloading]]"></paper-spinner>
            </app-toolbar>
          </app-header>
          
          <paper-tabs selected="{{page}}" noink>
            <paper-tab>Registro</paper-tab>
            <paper-tab>Activación</paper-tab>
            <paper-tab>Acceso</paper-tab>
            <paper-tab>Usuario</paper-tab>
            <paper-tab>Cuentas</paper-tab>
            <paper-tab>Transacciones</paper-tab>
          </paper-tabs>

          <iron-pages selected="[[page]]">
            <customer-register 
              api-base-url="[[apiBaseUrl]]" 
              is-loading={{isloading}} 
              on-show-message="showMessage">
            </customer-register>
            <customer-activation 
              api-base-url="[[apiBaseUrl]]" 
              is-loading={{isloading}} 
              on-show-message="showMessage">
            </customer-activation>
            <customer-login 
              api-base-url="[[apiBaseUrl]]" 
              is-loading={{isloading}} 
              id-session="{{session}}" 
              on-show-message="showMessage">
            </customer-login>
            <customer-info 
              api-base-url="[[apiBaseUrl]]" 
              is-loading={{isloading}} 
              id-session="[[session]]"
              obj-customer="{{customer}}" 
              on-show-message="showMessage">
            </customer-info>
            <accounts-mod 
              api-base-url="[[apiBaseUrl]]" 
              is-loading={{isloading}} 
              id-session="[[session]]" 
              obj-customer="[[customer]]">
            </accounts-mod>
          </iron-pages>

          <paper-dialog id="modal">
            <p>{{textModal}}</p>
            <div class="buttons">
              <paper-button dialog-confirm autofocus>Aceptar</paper-button>
            </div>
          </paper-dialog>

        </app-header-layout>
      </app-drawer-layout>
      
    `;
  }
  static get is() { return 'practitioner-front-app'; }
  static get properties() {
    return {
      session: { type: String,  notify: true, observer: '_sessionChanged' },
      customer: { type: Object,  notify: true },
      textModal: { type: String,  notify: true },
      apiBaseUrl:  { type: String, notify: true }
    };
  }
  constructor() {
    super();
    var savedSessionApp = localStorage.getItem('sessionApp');
    console.log('Saved session:' + savedSessionApp);
    this.session = savedSessionApp;
    this.apiBaseUrl = 'http://localhost:3000/api/v1/'; // LOCAL
    //this.apiBaseUrl = 'http://192.168.222.134:3000/api/v1/'; // DOCKER
  }
  ready() {
    super.ready();
    console.log('practitioner-front-app is ready!');
  }
  showMessage(e){
    this.textModal = e.detail.textModal;
    this.$.modal.open();
  }
  _sessionChanged(newVal, oldVal) {
    console.log('newVal:' + newVal, 'oldVal:' + oldVal)
  }
}

window.customElements.define(PractitionerFrontApp.is, PractitionerFrontApp);
