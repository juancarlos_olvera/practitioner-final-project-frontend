import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-form/iron-form.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';

class CustomerInfo extends PolymerElement {
  static get template() {
    return html`
        <iron-form>
            <paper-input always-float-label label="ID Cliente" readonly=true value="[[objCustomer.id]]"></paper-input>    
            <paper-input always-float-label label="Nombres" value="{{objCustomer.firstName}}"></paper-input>
            <paper-input always-float-label label="Apellidos" value="{{objCustomer.lastName}}"></paper-input>
            <paper-input always-float-label label="Correo Electrónico" readonly=true value="[[objCustomer.email]]"></paper-input>
            <paper-button raised on-click="getCustomer">Consultar</paper-button>
            <paper-button raised on-click="updateCustomer">Actualizar</paper-button>
        </iron-form>
    `;
  }
  static get is() { return 'customer-info'; }
  static get properties() {
    return {
        objCustomer: { type: Object, value: function() { return {} }, notify: true },
        idSession:   { type: String },
        isLoading:   { type: Boolean, notify: true },
        apiBaseUrl : { type: String }
    }
  }
  updateCustomer() {
    this.isLoading = true;  
    console.log('updateCustomer(input): ' + JSON.stringify(this.objCustomer));
    var url = this.apiBaseUrl + 'customers/' + this.objCustomer.id;
    var appHeaders = new Headers();
    appHeaders.append("Content-Type", "application/json");
    appHeaders.append("X-App-Session", this.idSession);
    let _this = this;
    fetch(url, {
        method: 'PUT', 
        body: JSON.stringify(this.objCustomer), 
        headers:appHeaders
    }).then(function(response) {
        if(response.ok) {
            return response.json();
        }
        throw new Error('Network response was not ok.');
    }).then(function(responseJson) { 
        console.log('updateCustomer(response): ' + JSON.stringify(responseJson));
        _this.objCustomer = responseJson;
        _this.dispatchEvent(
            new CustomEvent('show-message', {
                detail: {
                    textModal: 'Modificación realizada!'
                }
            }
        ));
        _this.isLoading = false;
    }).catch(function(error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
        _this.isLoading = false;
    });
  }
  getCustomer() {
    this.isLoading = true;
    console.log('getCustomer(input): ' + this.idSession);
    var url = this.apiBaseUrl + 'customers/me';
    var appHeaders = new Headers();
    appHeaders.append("Content-Type", "application/json");
    appHeaders.append("X-App-Session", this.idSession);
    let _this = this;
    fetch(url, {
        method: 'GET', 
        headers:appHeaders
    }).then(function(response) {
        if(response.ok) {
            return response.json();
        }
        throw new Error('Network response was not ok.');
    }).then(function(responseJson) { 
        console.log('getCustomer(response): ' + JSON.stringify(responseJson));
        _this.objCustomer = responseJson;
        _this.isLoading = false;
    }).catch(function(error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
        _this.isLoading = false;
    });
  }
}
customElements.define(CustomerInfo.is, CustomerInfo);